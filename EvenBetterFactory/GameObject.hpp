#ifndef _GAMEOBJECT_HPP_
#define _GAMEOBJECT_HPP_

class GameObject
{
    public:
    GameObject();
    virtual ~GameObject();

    /* Pure Virtuals */
    virtual void draw() = 0;
    virtual void update() = 0;
};

#endif
